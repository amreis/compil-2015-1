#include "cc_optimize.h"
#include <string.h>

void dfs_mark_set(comp_list_item_t* instr, comp_list_item_t* forbidden_instr, int forbidden_set, int set)
{
    if(instr == NULL) return;
    if(instr == (comp_list_item_t*)-1) return;
    if(instr->last_known_set == set) return;
    if(instr == forbidden_instr) return;
    if(instr->last_known_set == forbidden_set) return;
    instr->last_known_set = set;
    dfs_mark_set(instr->flow1, forbidden_instr, forbidden_set, set);
    dfs_mark_set(instr->flow2, forbidden_instr, forbidden_set, set);
}

void dfs_switch_label(comp_list_item_t* instr, comp_list_item_t* orig_label, int forbidden_set, int set)
{
    if(instr == NULL) return;
    if(instr == (comp_list_item_t*)-1) return;
    if(instr->last_known_set == set) return;
    if(instr == orig_label) return;
    if(instr->last_known_set == forbidden_set) return;
    instr->last_known_set = set;
    if(instr->flow1 == orig_label || instr->flow2 == orig_label)
    {
        //TODO: switch instr->instr label from orig_label->instr to orig_label->instr + _loop, which is the same as orig_label->next->instr
        if(instr->flow1 == orig_label) instr->flow1 = orig_label->next;
        if(instr->flow2 == orig_label) instr->flow2 = orig_label->next;
    }
    dfs_switch_label(instr->flow1, orig_label, forbidden_set, set);
    dfs_switch_label(instr->flow2, orig_label, forbidden_set, set);
}

int dfs_test_const(comp_list_item_t* instr, comp_list_item_t* base_instr, int forbidden_set, int set)
{
    if(instr == NULL) return 1;
    if(instr == (comp_list_item_t*)-1) return 1;
    if(instr->last_known_set == set+1) return 1;
    if(instr->last_known_set == forbidden_set) return 1;
    int prev_set = instr->last_known_set;
    instr->last_known_set = set+1;
    int ret = 0; //TODO: test if instr assigns to base_instr's inputs, or if instr!=base_instr && both assign to the same var
    ret *= dfs_test_const(instr->flow1, base_instr, forbidden_set, set);
    ret *= dfs_test_const(instr->flow2, base_instr, forbidden_set, set);
    instr->last_known_set = prev_set;
    return ret;
}

int dfs_test_dominates(comp_list_item_t* instr, comp_list_item_t* base_instr, int forbidden_set, int set)
{
    if(instr == NULL) return 1;
    if(instr == (comp_list_item_t*)-1) return 1;
    if(instr == base_instr) return 1;
    if(instr->last_known_set == set+1) return 1;
    if(instr->last_known_set == forbidden_set) return 1;
    int prev_set = instr->last_known_set;
    instr->last_known_set = set+1;
    int ret = 0; //TODO: test if base_instr assigns to instr's inputs
    ret *= dfs_test_dominates(instr->flow1, base_instr, forbidden_set, set);
    ret *= dfs_test_dominates(instr->flow2, base_instr, forbidden_set, set);
    instr->last_known_set = prev_set;
    return ret;
}

int dfs_optimize_loops(comp_list_item_t* instr, comp_list_item_t* prev, comp_list_item_t* last_const, int forbidden_set, int set)
{
    if(instr == NULL) return 0;
    if(instr == (comp_list_item_t*)-1) return 0;
    if(instr->last_known_set == set) return 0;
    if(instr->last_known_set == forbidden_set) return 0;
    instr->last_known_set = set;
    int ret = 0;
    if(dfs_test_const(last_const->next, instr, forbidden_set, set) && dfs_test_dominates(last_const->next, instr, forbidden_set, set))
    {
        prev->next = instr->next;
        if(prev->flow1 == instr) prev->flow1 = prev->next;
        if(prev->flow2 == instr) prev->flow2 = prev->next;
        instr->next = instr->flow1 = last_const->next;
        last_const->next = last_const->flow1 = instr;
        instr = prev;
        ret++;
    }
    ret += dfs_optimize_loops(instr->flow1, instr, last_const, forbidden_set, set);
    ret += dfs_optimize_loops(instr->flow2, instr, last_const, forbidden_set, set);
    return ret;
}

void optimize(comp_list_t* code)
{
    comp_list_item_t* label;
    int label_set = 1;
    for(label = code->start; label != NULL; label = label->next)
    {
        int label_length = strlen(label->instr);
        if(label->instr[label_length-1] == ':') //if actually a label
        {
            dfs_mark_set(code->start, label, -1, label_set);
            int cur_set = label_set+1;
            dfs_mark_set(label->flow1, NULL, label_set, cur_set);
            if(label->last_known_set == cur_set) //if a loop
            {
                //label: turns into label: const instructions (last_const) loop_label:
                char loop_string[30];
                strcpy(loop_string,label->instr);
                strcpy(loop_string+label_length-1,"_loop:");
                comp_list_item_t* loop_label = new_list_item_valued(loop_string);
                loop_label->next = loop_label->flow1 = label->next;
                label->next = label->flow1 = loop_label;
                cur_set++;
                dfs_switch_label(loop_label->next, label, label_set, cur_set);
                comp_list_item_t* last_const = label;
                do {
                    while(last_const->next != loop_label)
                        last_const = last_const->next;
                    cur_set++;
                } while(dfs_optimize_loops(loop_label, last_const, last_const, label_set, cur_set));
                label = loop_label;
            }
            label_set = cur_set + 1;
        }
    }
}
