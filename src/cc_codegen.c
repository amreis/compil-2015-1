#include "cc_codegen.h"
#include "cc_stack.h"
#include "cc_semantic.h"
#include "cc_optimize.h"

extern comp_stack_t* sym_stack;
extern comp_dict_item_t* current_function;
unsigned int next_reg = 0;

void gera_reg(char* out)
{
    sprintf(out, "r%d", next_reg++);
    return;
}

unsigned int next_rot = 0;

void gera_rot(char* out)
{
    sprintf(out, "l%d", next_rot++);
    return;
}

comp_list_t* gen_literal(comp_tree_t* node)
{
    char reg[20], instr[100];
    gera_reg(reg);

    if (node->value->token_type == SIMBOLO_LITERAL_INT ||
        node->value->token_type == SIMBOLO_LITERAL_FLOAT)
    {
        sprintf(instr, "loadI %d => %s", node->value->token_val.int_val, reg);
    }
    else if (node->value->token_type == SIMBOLO_LITERAL_BOOL)
    {
        sprintf(instr, "loadI %d => %s", node->value->token_val.bool_val, reg);
    }
    else
    {
        return NULL;
    }

    comp_list_t* list = new_list();
    // fprintf(stderr, "%s\n", instr);
    comp_list_item_t* i = new_list_item_valued(instr);
    append_instr(list, i);
    node->code = list;
    node->reg_result = strdup(reg);
    return list;
}

comp_list_t* gen_arim_binaria(comp_tree_t* node)
{
    char reg[20], instr[100];
    if (node->child[0] == NULL || node->child[1] == NULL)
        return NULL;
    strcpy(reg, node->child[0]->reg_result);
    char *reg_dir = node->child[1]->reg_result;
    switch (node->type)
    {
        case AST_ARIM_SOMA:
        sprintf(instr, "add %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_ARIM_SUBTRACAO:
        sprintf(instr, "sub %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_ARIM_MULTIPLICACAO:
        sprintf(instr, "mult %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_ARIM_DIVISAO:
        sprintf(instr, "div %s, %s => %s", reg, reg_dir, reg);
    }
    node->code = concat_list(node->child[0]->code, node->child[1]->code);
    comp_list_item_t* i = new_list_item_valued(instr);
    append_instr(node->code, i);
    node->reg_result = strdup(reg);
    // fprintf(stderr, "%s\n", instr);
    return node->code;
}

comp_list_t* gen_arim_unaria(comp_tree_t* node)
{
    char reg[20], instr[100];
    if (node->child[0] == NULL)
        return NULL;
    strcpy(reg, node->child[0]->reg_result);
    switch (node->type)
    {
        case AST_ARIM_INVERSAO:
        sprintf (instr, "rsubI %s, 0 => %s", reg, reg);
        break;
    }
    node->code = node->child[0]->code;
    comp_list_item_t* i = new_list_item_valued(instr);
    append_instr(node->code, i);
    node->reg_result = strdup(reg);
    // fprintf(stderr, "%s\n", instr);
    return node->code;
}

comp_list_t* gen_logic_comp(comp_tree_t* node)
{
    char reg[20], instr[100];
    if (node->child[0] == NULL || node->child[1] == NULL)
        return NULL;
    strcpy(reg, node->child[0]->reg_result);
    char *reg_dir = node->child[1]->reg_result;
    switch (node->type)
    {
        case AST_LOGICO_COMP_DIF:
        sprintf (instr, "cmp_NE %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_LOGICO_COMP_IGUAL:
        sprintf (instr, "cmp_EQ %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_LOGICO_COMP_LE:
        sprintf (instr, "cmp_LE %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_LOGICO_COMP_GE:
        sprintf (instr, "cmp_GE %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_LOGICO_COMP_L:
        sprintf (instr, "cmp_LT %s, %s => %s", reg, reg_dir, reg);
        break;
        case AST_LOGICO_COMP_G:
        sprintf (instr, "cmp_GT %s, %s => %s", reg, reg_dir, reg);
        break;
    }
    node->code = concat_list(node->child[0]->code, node->child[1]->code);
    comp_list_item_t* i = new_list_item_valued(instr);
    append_instr(node->code, i);
    node->reg_result = strdup(reg);
    //fprintf(stderr, "%s\n", instr);
    return node->code;
}

comp_list_t* gen_logic_unaria(comp_tree_t* node)
{
    char reg[20], instr[100];
    if (node->child[0] == NULL)
        return NULL;
    strcpy(reg, node->child[0]->reg_result);
    char reg_dir[20]; gera_reg(reg_dir);
    node->code = node->child[0]->code;
    switch (node->type)
    {
        case AST_LOGICO_COMP_NEGACAO:
        sprintf (instr, "cmp_NE %s, %s => %s", reg_dir, reg_dir, reg_dir); // reg_dir = false
        append_instr(node->code, new_list_item_valued(instr));
        sprintf (instr, "cmp_EQ %s, %s => %s", reg, reg_dir, reg);
        append_instr(node->code, new_list_item_valued(instr));
        break;
    }
    node->reg_result = strdup(reg);
    // fprintf(stderr, "%s\n", instr);
    return node->code;
}

comp_list_t* gen_logic_binaria(comp_tree_t* node)
{
    char reg[20], instr[100], rotcalc[20], rotshort[20];
    if (node->child[0] == NULL || node->child[1] == NULL)
        return NULL;
    strcpy(reg, node->child[0]->reg_result);
    char *reg_dir = node->child[1]->reg_result;
    gera_rot(rotcalc); gera_rot(rotshort);

    node->code = node->child[0]->code;
    if(node->type == AST_LOGICO_E)
        sprintf (instr, "cbr %s => %s, %s", reg, rotcalc, rotshort);
    else if(node->type == AST_LOGICO_OU)
        sprintf (instr, "cbr %s => %s, %s", reg, rotshort, rotcalc);
    comp_list_item_t* branch_item = new_list_item_valued(instr);
    append_instr(node->code, branch_item);
    sprintf (instr, "%s:", rotcalc);
    append_instr(node->code, new_list_item_valued(instr));
    node->code = concat_list(node->code, node->child[1]->code);
    sprintf (instr, "i2i %s => %s", reg_dir, reg);
    append_instr(node->code, new_list_item_valued(instr));
    sprintf (instr, "%s:", rotshort);
    append_instr(node->code, new_list_item_valued(instr));
    branch_item->flow2 = node->code->end;

    node->reg_result = strdup(reg);
    return node->code;
}

comp_list_t* gen_if_else(comp_tree_t* node)
{
    char reg[20], instr[100], rottrue[20], rotfalse[20], rotnext[20];
    if (node->child[0] == NULL || node->child[1] == NULL)
        return NULL;
    gera_rot(rottrue);
    gera_rot(rotfalse);

    if (node->child[2] == NULL)
    {
        // if-then
        strcpy(reg, node->child[0]->reg_result);
        node->code = node->child[0]->code;
        sprintf (instr, "cbr %s => %s, %s", reg, rottrue, rotfalse);
        comp_list_item_t* branch_item = new_list_item_valued(instr);
        append_instr(node->code, branch_item);
        sprintf(instr, "%s:", rottrue);
        append_instr(node->code, new_list_item_valued(instr));
        node->code = concat_list(node->code, node->child[1]->code);
        sprintf(instr, "%s:", rotfalse);
        append_instr(node->code, new_list_item_valued(instr));
        branch_item->flow2 = node->code->end;
    }
    else
    {
        // if-then-else
        gera_rot(rotnext);
        strcpy(reg, node->child[0]->reg_result);
        node->code = node->child[0]->code;
        sprintf (instr, "cbr %s => %s, %s", reg, rottrue, rotfalse);
        comp_list_item_t* branch_item = new_list_item_valued(instr);
        append_instr(node->code, branch_item);
        sprintf(instr, "%s:", rottrue);
        append_instr(node->code, new_list_item_valued(instr));
        node->code = concat_list(node->code, node->child[1]->code);
        sprintf(instr, "jumpI -> %s", rotnext);
        comp_list_item_t* jump_item = new_list_item_valued(instr);
        append_instr(node->code, jump_item);
        sprintf(instr, "%s:", rotfalse);
        append_instr(node->code, new_list_item_valued(instr));
        branch_item->flow2 = node->code->end;
        if(node->child[1]->type == AST_BLOCO && node->child[2]->child[0] != NULL)
            node->code = concat_list(node->code, node->child[2]->child[0]->code);
        else
            node->code = concat_list(node->code, node->child[2]->code);
        sprintf(instr, "%s:", rotnext);
        append_instr(node->code, new_list_item_valued(instr));
        jump_item->flow1 = node->code->end;
    }

    node->reg_result = strdup(reg);
    return node->code;
}

comp_list_t* gen_while_do(comp_tree_t* node)
{
    char reg[20], instr[100], rottrue[20], rotfalse[20], rotbegin[20];
    if (node->child[0] == NULL || node->child[1] == NULL)
        return NULL;
    gera_rot(rotbegin);
    gera_rot(rottrue);
    gera_rot(rotfalse);

    node->code = new_list();
    sprintf(instr, "%s:", rotbegin);
    append_instr(node->code, new_list_item_valued(instr));
    comp_list_item_t* begin_item = node->code->end;
    node->code = concat_list(node->code, node->child[0]->code);
    strcpy(reg, node->child[0]->reg_result);
    sprintf (instr, "cbr %s => %s, %s", reg, rottrue, rotfalse);
    comp_list_item_t* branch_item = new_list_item_valued(instr);
    append_instr(node->code, branch_item);
    sprintf(instr, "%s:", rottrue);
    append_instr(node->code, new_list_item_valued(instr));
    node->code = concat_list(node->code, node->child[1]->code);
    sprintf(instr, "jumpI -> %s", rotbegin);
    append_instr(node->code, new_list_item_valued(instr));
    node->code->end->flow1 = begin_item;
    sprintf(instr, "%s:", rotfalse);
    append_instr(node->code, new_list_item_valued(instr));
    branch_item->flow2 = node->code->end;

    node->reg_result = strdup(reg);
    return node->code;
}

comp_list_t* gen_do_while(comp_tree_t* node)
{
    char reg[20], instr[100], rottrue[20], rotfalse[20], rotbegin[20];
    if (node->child[0] == NULL || node->child[1] == NULL)
        return NULL;
    gera_rot(rotbegin);
    gera_rot(rottrue);
    gera_rot(rotfalse);

    node->code = new_list();
    sprintf(instr, "%s:", rottrue);
    append_instr(node->code, new_list_item_valued(instr));
    comp_list_item_t* true_item = node->code->end;
    node->code = concat_list(node->code, node->child[0]->code);
    node->code = concat_list(node->code, node->child[1]->code);
    strcpy(reg, node->child[1]->reg_result);
    sprintf (instr, "cbr %s => %s, %s", reg, rottrue, rotfalse);
    append_instr(node->code, new_list_item_valued(instr));
    node->code->end->flow2 = true_item;
    sprintf(instr, "%s:", rotfalse);
    append_instr(node->code, new_list_item_valued(instr));

    node->reg_result = strdup(reg);
    return node->code;
}


comp_list_t* gen_atribuicao(comp_tree_t* node)
{
    char reg_access[20];
    if (node->child[0] == NULL || node->child[1] == NULL)
        return NULL;
    char instr[100];
    comp_tree_t *target_node = node;
    if (node->child[0]->type == AST_VETOR_INDEXADO)
    {
        target_node = node->child[0];
        comp_tree_t* args_list = target_node->child[1];
        target_node->code = concat_list(target_node->code, args_list->code);
        strcpy(reg_access, args_list->reg_result);
        args_list = args_list->next;
        int i = 1;
        comp_dict_item_t* var = target_node->child[0]->value;
        for (; args_list != NULL; args_list = args_list->next)
        {
            sprintf(instr, "multI %s, %d => %s", reg_access, var->type.dim_sizes[i], reg_access);
            i++;
            append_instr(target_node->code, new_list_item_valued(instr));
            sprintf(instr, "add %s, %s => %s", reg_access, args_list->reg_result, reg_access);
            append_instr(target_node->code, new_list_item_valued(instr));
        }
        sprintf(instr, "mult %s, %d => %s", reg_access, size_of_base(var->type.base), reg_access);
        append_instr(target_node->code, new_list_item_valued(instr));
        sprintf(instr, "addI %s, %d => %s", reg_access, var->addr.base, reg_access);
        append_instr(target_node->code, new_list_item_valued(instr));
        if (var->addr.scope == SCOPE_GLOBAL)
        {
            sprintf(instr, "storeAO %s => rbss, %s", node->child[1]->reg_result, reg_access);
            append_instr(target_node->code, new_list_item_valued(instr));
        }
        else
        {
            sprintf(instr, "storeAO %s => rarp, %s", node->child[1]->reg_result, reg_access);
            append_instr(target_node->code, new_list_item_valued(instr));
        }
        node->code = node->child[1]->code;
        node->code = concat_list(node->code, target_node->code);
        return node->code;
    }
    else {
        if (node->child[0]->value->addr.scope == SCOPE_GLOBAL)
            sprintf(instr, "storeAI %s => rbss, %d", node->child[1]->reg_result, node->child[0]->value->addr.base);
        else {
            sprintf(instr, "storeAI %s => rarp, %d", node->child[1]->reg_result, node->child[0]->value->addr.base);
        }
    }
    free_list(node->child[0]->code);
    node->code = node->child[1]->code;
    append_instr(node->code, new_list_item_valued(instr));
    node->reg_result = NULL;
    // fprintf(stderr, "%s\n", instr);
    return node->code;
}

comp_list_t* gen_identificador(comp_tree_t* node)
{
    char reg[20], instr[100];
    gera_reg(reg);
    if (node->value->addr.scope == SCOPE_GLOBAL)
    {
        sprintf(instr, "loadAI rbss, %d => %s", node->value->addr.base, reg);
    } else {
        sprintf(instr, "loadAI rarp, %d => %s", node->value->addr.base, reg);
    }
    // fprintf(stderr, "%s\n", instr);
    comp_list_t* list = new_list();
    append_instr(list, new_list_item_valued(instr));
    node->code = list;
    node->reg_result = strdup(reg);
    return list;
}

comp_list_t* gen_le_vetor(comp_tree_t* node)
{
    char reg_access[20], instr[100], reg[20];
    gera_reg(reg);
    comp_dict_item_t* var = node->child[0]->value;
    comp_tree_t* args_list = node->child[1];
    node->code = concat_list(node->code, args_list->code);
    strcpy(reg_access, args_list->reg_result);
    args_list = args_list->next;
    int i = 1;
    for (; args_list != NULL; args_list = args_list->next)
    {
        sprintf(instr, "multI %s, %d => %s", reg_access, var->type.dim_sizes[i++], reg_access);
        append_instr(node->code, new_list_item_valued(instr));
        sprintf(instr, "add %s, %s => %s", reg_access, args_list->reg_result, reg_access);
        append_instr(node->code, new_list_item_valued(instr));
    }
    sprintf(instr, "mult %s, %d => %s", reg_access, size_of_base(var->type.base), reg_access);
    append_instr(node->code, new_list_item_valued(instr));
    sprintf(instr, "addI %s, %d => %s", reg_access, var->addr.base, reg_access);
    append_instr(node->code, new_list_item_valued(instr));
    if (var->addr.scope == SCOPE_GLOBAL)
    {
        sprintf(instr, "loadAO rbss, %s => %s", reg_access, reg);
        append_instr(node->code, new_list_item_valued(instr));
    }
    else
    {
        sprintf(instr, "loadAO rarp, %s => %s", reg_access, reg);
        append_instr(node->code, new_list_item_valued(instr));
    }
    node->reg_result = strdup(reg);
    return node->code;
}

void print_code(comp_list_t* code)
{
    if (code == NULL) return;
    comp_list_item_t* s = code->start;
    while (s != NULL)
    {
        printf("%s\n", s->instr);
        s = s->next;
    }
}
#define DEBUG 1

comp_list_t* gen_funcao(comp_tree_t* node)
{
    char instr[100];
    comp_dict_item_t* val = node->value;
    node->code = new_list();
    #if DEBUG
    sprintf(instr, "// START %s:", val->lex);
    append_instr(node->code, new_list_item_valued(instr));
    #endif
    // generate initial label
    sprintf(instr, "LF%s:", val->lex);
    append_instr(node->code, new_list_item_valued(instr));
    node->code = concat_list(node->code, val->init_code);
    val->init_code = NULL;
    if (node->child[0] != NULL)
        node->code = concat_list(node->code, node->child[0]->code);

    append_instr(node->code, new_list_item_valued("loadAI rarp, 8 => rsp"));
    append_instr(node->code, new_list_item_valued("loadAI rarp, 4 => rarp"));
    append_instr(node->code, new_list_item_valued("loadAI rsp, 0 => ret"));
    append_instr(node->code, new_list_item_valued("jump -> ret"));
    node->code->end->flow1 = (comp_list_item_t*) -1;
    #if DEBUG
    sprintf(instr, "// END %s:", val->lex);
    append_instr(node->code, new_list_item_valued(instr));
    #endif
    return node->code;
}
comp_list_t* gen_chamada_func(comp_tree_t* node)
{
    char instr[100];
    comp_dict_item_t* func = node->child[0]->value;
    comp_tree_t* args = node->child[1];
    node->code = new_list();
    #if DEBUG
    sprintf(instr, "// START CALL %s:", func->lex);
    append_instr(node->code, new_list_item_valued(instr));
    #endif
    if (node->child[1] != NULL)
        node->code = concat_list(node->code, node->child[1]->code);
    // replace CALL in second pass to get return addr
    append_instr(node->code, new_list_item_valued("CALL"));
    append_instr(node->code, new_list_item_valued("storeAI rarp => rsp, 4"));
    append_instr(node->code, new_list_item_valued("storeAI rsp => rsp, 8"));
    int where = 12;
    comp_tree_t* arg;
    for (arg = args; arg != NULL; arg = arg->next)
    {
        sprintf(instr, "storeAI %s => rsp, %d", arg->reg_result, where);
        append_instr(node->code, new_list_item_valued(instr));
        where += size_of_base(arg->semantic_type);
    }
    append_instr(node->code, new_list_item_valued("i2i rsp => rarp"));
    sprintf(instr, "addI rsp, %d => rsp", func->type.frame_size);
    append_instr(node->code, new_list_item_valued(instr));
    sprintf(instr, "jumpI -> LF%s", func->lex);
    node->code->end->flow1 = (comp_list_item_t*) -1; //TODO: like this because we can't find a loop here without inlining the function
    append_instr(node->code, new_list_item_valued(instr));
    append_instr(node->code, new_list_item_valued("RET:"));
    char reg[20];
    gera_reg(reg);
    node->reg_result = strdup(reg);
    sprintf(instr, "loadAI rsp, %d => %s", func->type.frame_size - size_of_base(func->type.base), reg);
    append_instr(node->code, new_list_item_valued(instr));
    #if DEBUG
    sprintf(instr, "// END CALL %s:", func->lex);
    append_instr(node->code, new_list_item_valued(instr));
    #endif
    return node->code;
}

comp_list_t* gen_programa(comp_tree_t *node)
{
    node->code = new_list();
    append_instr(node->code, new_list_item_valued("loadI 0 => rarp"));
    append_instr(node->code, new_list_item_valued("loadI 0 => rsp"));
    append_instr(node->code, new_list_item_valued("loadI 0 => rbss"));
    // main's prologue
    comp_dict_item_t* main = query_stack_function(sym_stack, "main");
    char instr[100];
    sprintf(instr, "loadI %d => rsp", main->type.frame_size);
    append_instr(node->code, new_list_item_valued(instr));
    append_instr(node->code, new_list_item_valued("loadI 10 => r0"));
    append_instr(node->code, new_list_item_valued("storeAI r0 => rarp, 0"));
    append_instr(node->code, new_list_item_valued("loadI 0 => r0"));
    append_instr(node->code, new_list_item_valued("storeAI r0 => rarp, 4"));
    append_instr(node->code, new_list_item_valued("storeAI r0 => rarp, 8"));
    append_instr(node->code, new_list_item_valued("jumpI -> LFmain"));
    node->code->end->flow1 = (comp_list_item_t*) -1;
    append_instr(node->code, new_list_item_valued("jumpI -> L_END"));
    node->code->end->flow1 = (comp_list_item_t*) -1;
    if (node->child[0] != NULL)
        node->code = concat_list(node->code, node->child[0]->code);
    append_instr(node->code, new_list_item_valued("L_END:"));
    node->code->end->flow1 = (comp_list_item_t*) -1; //TODO
    return node->code;
}

comp_list_t* gen_return(comp_tree_t* node)
{
    node->code = node->child[0]->code;
    int where = current_function->type.frame_size - size_of_base(current_function->type.base);
    char instr[100];
    sprintf(instr, "storeAI %s => rarp, %d", node->child[0]->reg_result, where);
    append_instr(node->code, new_list_item_valued(instr));
    return node->code;
}

void solve_calls(comp_list_t* code)
{
    char instr[100], reg[20];
    if (code == NULL) return;
    int pc = 0;
    comp_list_item_t* elem = code->start;
    comp_list_item_t* ant = NULL;
    if (elem == NULL) return;
    comp_list_item_t* save_ret = NULL;
    comp_list_item_t* ant_save_ret = NULL;
    int ret_addr = -1;
    enum { WAITING_CALL, WAITING_RET } status = WAITING_CALL;
    for (; elem != NULL; ant = elem, elem = elem->next)
    {
        int l = strlen(elem->instr);
        if (elem->instr[l-1] != ':') // not a label
        {
            pc++;
        }
        if (status == WAITING_CALL && strcmp(elem->instr, "CALL") == 0)
        {
            save_ret = elem;
            ant_save_ret = ant;
            status = WAITING_RET;
        }
        else if (status == WAITING_RET && strcmp(elem->instr, "RET:") == 0)
        {
            gera_reg(reg);
            ret_addr = pc + 1;
            free(save_ret->instr);

            sprintf(instr, "loadI %d => %s", ret_addr, reg);
            comp_list_item_t* new = new_list_item_valued(instr);
            sprintf(instr, "storeAI %s => rsp, 0", reg);
            save_ret->instr = strdup(instr);
            status = WAITING_CALL;

            ant_save_ret->next = new;
            new->next = save_ret;

            save_ret = NULL;
            
            pc++;
            
            ant->next = elem->next;
            free(elem->instr);
            free(elem);
        }
    }
}

void second_pass(comp_tree_t* node)
{
    optimize(node->code);
    solve_calls(node->code);
}

comp_list_t* gen_code(comp_tree_t* node)
{
    if (node == NULL) return NULL;
    switch(node->type)
    {
        case AST_LITERAL:
            return gen_literal(node);
        case AST_ARIM_SOMA:
        case AST_ARIM_SUBTRACAO:
        case AST_ARIM_MULTIPLICACAO:
        case AST_ARIM_DIVISAO:
            return gen_arim_binaria(node);
        case AST_ARIM_INVERSAO:
            return gen_arim_unaria(node);
        case AST_LOGICO_COMP_DIF:
        case AST_LOGICO_COMP_IGUAL:
        case AST_LOGICO_COMP_LE:
        case AST_LOGICO_COMP_GE:
        case AST_LOGICO_COMP_L:
        case AST_LOGICO_COMP_G:
            return gen_logic_comp(node);
        case AST_LOGICO_E:
        case AST_LOGICO_OU:
            return gen_logic_binaria(node);
        case AST_LOGICO_COMP_NEGACAO:
            return gen_logic_unaria(node);
        case AST_IDENTIFICADOR:
            return gen_identificador(node);
        case AST_VETOR_INDEXADO:
            return gen_le_vetor(node);
        case AST_ATRIBUICAO:
            return gen_atribuicao(node);
        case AST_CHAMADA_DE_FUNCAO:
            return gen_chamada_func(node);
        case AST_PROGRAMA:
            return gen_programa(node);
        case AST_FUNCAO:
            return gen_funcao(node);
        case AST_BLOCO:
            node->code = (node->child[0] == NULL ? NULL : node->child[0]->code);
            return node->code;
        case AST_IF_ELSE:
            return gen_if_else(node);
        case AST_WHILE_DO:
            return gen_while_do(node);
        case AST_DO_WHILE:
            return gen_do_while(node);
        case AST_RETURN:
            return gen_return(node);
        default:
            return NULL;
    }
}
