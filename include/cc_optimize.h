#ifndef OPTIMIZE_H
#define OPTIMIZE_H

#include "cc_list.h"

void optimize(comp_list_t* code);

#endif
